//
//  TemplateConnector_tvOS.h
//  TemplateConnector_tvOS
//
//  Created by Berrie Kremers on 02/12/2018.
//  Copyright © 2018 Katoemba Software. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TemplateConnector_tvOS.
FOUNDATION_EXPORT double TemplateConnector_tvOSVersionNumber;

//! Project version string for TemplateConnector_tvOS.
FOUNDATION_EXPORT const unsigned char TemplateConnector_tvOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TemplateConnector_tvOS/PublicHeader.h>


