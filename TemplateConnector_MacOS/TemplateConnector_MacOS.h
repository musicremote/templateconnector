//
//  TemplateConnector_MacOS.h
//  TemplateConnector_MacOS
//
//  Created by Berrie Kremers on 02/12/2018.
//  Copyright © 2018 Katoemba Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for TemplateConnector_MacOS.
FOUNDATION_EXPORT double TemplateConnector_MacOSVersionNumber;

//! Project version string for TemplateConnector_MacOS.
FOUNDATION_EXPORT const unsigned char TemplateConnector_MacOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TemplateConnector_MacOS/PublicHeader.h>


