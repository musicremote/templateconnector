//
//  TemplateBrowse.swift
//  TemplateConnector
//
//  Created by Berrie Kremers on 10/03/2019.
//  Copyright © 2019 Katoemba Software. All rights reserved.
//

import Foundation
import ConnectorProtocol
import RxSwift
import RxCocoa

public class TemplateBrowse: BrowseProtocol {
    public func songsByArtist(_ artist: Artist) -> Observable<[Song]> {
        return Observable.empty()
    }
    
    public func albumsByArtist(_ artist: Artist, sort: SortType) -> Observable<[Album]> {
        return Observable.empty()
    }
    
    public func songsOnAlbum(_ album: Album) -> Observable<[Song]> {
        return Observable.empty()
    }
    
    public func songsInPlaylist(_ playlist: Playlist) -> Observable<[Song]> {
        return Observable.empty()
    }
    
    public func search(_ search: String, limit: Int, filter: [SourceType]) -> Observable<SearchResult> {
        return Observable.empty()
    }
    
    public func albumSectionBrowseViewModel() -> AlbumSectionBrowseViewModel {
        return TemplateAlbumSectionBrowseViewModel()
    }
    
    public func albumBrowseViewModel() -> AlbumBrowseViewModel {
        return TemplateAlbumBrowseViewModel()
    }
    
    public func albumBrowseViewModel(_ artist: Artist) -> AlbumBrowseViewModel {
        return TemplateAlbumBrowseViewModel()
    }
    
    public func albumBrowseViewModel(_ genre: Genre) -> AlbumBrowseViewModel {
        return TemplateAlbumBrowseViewModel()
    }
    
    public func albumBrowseViewModel(_ albums: [Album]) -> AlbumBrowseViewModel {
        return TemplateAlbumBrowseViewModel()
    }
    
    public func artistBrowseViewModel(type: ArtistType) -> ArtistBrowseViewModel {
        return TemplateArtistBrowseViewModel()
    }
    
    public func artistBrowseViewModel(_ genre: Genre, type: ArtistType) -> ArtistBrowseViewModel {
        return TemplateArtistBrowseViewModel()
    }
    
    public func artistBrowseViewModel(_ artists: [Artist]) -> ArtistBrowseViewModel {
        return TemplateArtistBrowseViewModel()
    }
    
    public func playlistBrowseViewModel() -> PlaylistBrowseViewModel {
        return TemplatePlaylistBrowseViewModel()
    }
    
    public func playlistBrowseViewModel(_ playlists: [Playlist]) -> PlaylistBrowseViewModel {
        return TemplatePlaylistBrowseViewModel()
    }
    
    public func songBrowseViewModel(_ songs: [Song]) -> SongBrowseViewModel {
        return TemplateSongBrowseViewModel()
    }
    
    public func songBrowseViewModel(_ album: Album, artist: Artist?) -> SongBrowseViewModel {
        return TemplateSongBrowseViewModel()
    }
    
    public func songBrowseViewModel(_ playlist: Playlist) -> SongBrowseViewModel {
        return TemplateSongBrowseViewModel()
    }
    
    public func songBrowseViewModel(random: Int) -> SongBrowseViewModel {
        return TemplateSongBrowseViewModel()
    }
    
    public func genreBrowseViewModel() -> GenreBrowseViewModel {
        return TemplateGenreBrowseViewModel()
    }
    
    public func folderContentsBrowseViewModel() -> FolderBrowseViewModel {
        return TemplateFolderBrowseViewModel()
    }
    
    public func folderContentsBrowseViewModel(_ parentFolder: Folder) -> FolderBrowseViewModel {
        return TemplateFolderBrowseViewModel()
    }
    
    public func artistFromSong(_ song: Song) -> Observable<Artist> {
        return Observable.empty()
    }
    
    public func albumFromSong(_ song: Song) -> Observable<Album> {
        return Observable.empty()
    }
    
    public func preprocessCoverURI(_ coverURI: CoverURI) -> Observable<CoverURI> {
        return Observable.empty()
    }
    
    public func imageDataFromCoverURI(_ coverURI: CoverURI) -> Observable<Data?> {
        return Observable.just(nil)
    }
    
    public func diagnostics(album: Album) -> Observable<String> {
        return Observable.empty()
    }
    
}
