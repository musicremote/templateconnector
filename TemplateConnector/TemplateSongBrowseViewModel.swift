//
//  TemplateSongBrowseViewModel.swift
//  TemplateConnector
//
//  Created by Berrie Kremers on 10/03/2019.
//  Copyright © 2019 Katoemba Software. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ConnectorProtocol

public class TemplateSongBrowseViewModel: SongBrowseViewModel {
    private var loadProgress = BehaviorRelay<LoadProgress>(value: .notStarted)
    public var loadProgressObservable: Observable<LoadProgress> {
        return loadProgress.asObservable()
    }

    private var songsSubject = PublishSubject<[Song]>()
    public var songsObservable: Observable<[Song]> {
        return songsSubject.asObservable()
    }
    public var songsWithSubfilterObservable: Observable<[Song]> {
        return songsObservable
            .map({ [weak self] (songs) -> [Song] in
                guard let weakSelf = self else { return songs }
                
                if let subFilter = weakSelf.subFilter, case let .artist(artist) = subFilter {
                    var filteredSongs = [Song]()
                    for song in songs {
                        if artist.type == .artist || artist.type == .albumArtist {
                            if song.albumartist.lowercased().contains(artist.name.lowercased()) || song.artist.lowercased().contains(artist.name.lowercased()) {
                                filteredSongs.append(song)
                            }
                        }
                        else if artist.type == .composer {
                            if song.composer.lowercased().contains(artist.name.lowercased()) {
                                filteredSongs.append(song)
                            }
                        }
                        else if artist.type == .performer {
                            if song.performer.lowercased().contains(artist.name.lowercased()) {
                                filteredSongs.append(song)
                            }
                        }
                    }
                    return filteredSongs
                }
                return songs
            })
    }

    public private(set) var filter: BrowseFilter?
    public private(set) var subFilter: BrowseFilter?

    public func load() {
    }
    
    public func extend() {
    }
    
    public func removeSong(at: Int) {
    }
    
}
