//
//  TemplateArtistBrowseViewModel.swift
//  TemplateConnector
//
//  Created by Berrie Kremers on 10/03/2019.
//  Copyright © 2019 Katoemba Software. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ConnectorProtocol

public class TemplateArtistBrowseViewModel: ArtistBrowseViewModel {
    private var loadProgress = BehaviorRelay<LoadProgress>(value: .notStarted)
    public var loadProgressObservable: Observable<LoadProgress> {
        return loadProgress.asObservable()
    }
    
    private var artistsSubject = PublishSubject<[Artist]>()
    public var artistsObservable: Observable<[Artist]> {
        return artistsSubject.asObservable()
    }
    
    private var artistSectionsSubject = PublishSubject<ArtistSections>()
    public var artistSectionsObservable: Observable<ArtistSections> {
        return artistSectionsSubject.asObservable()
    }
    
    public private(set) var filters = [BrowseFilter]()
    
    public private(set) var artistType = ArtistType.artist
    
    public func load() {
    }
    
    public func load(filters: [BrowseFilter]) {
    }
    
    public func extend() {
    }
    
}
